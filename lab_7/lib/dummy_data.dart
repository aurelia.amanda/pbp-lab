import 'package:lab_7/models/kutipan_penyemangat.dart';
import 'package:lab_7/models/kutipan_penyemangat_json.dart';

// ignore: non_constant_identifier_names
Kutipan_PenyemangatJson DUMMY_CATEGORIES = Kutipan_PenyemangatJson(
  model: 'Kutipan_Penyemangat.Kutipan_Penyemangat',
  pk: 1,
  fields: [
    Kutipan_Penyemangat(
      nama: "Kelly",
      kutipan: 'Yuk bisa yuk, semangat terus ya!',
    ),
    Kutipan_Penyemangat(
      nama: "Dean",
      kutipan: 'Ngeluh boleh, capek juga boleh, bosen? boooleh, Nyerahh? jangan',
    ),
    Kutipan_Penyemangat(
      nama: "Dean lagi",
      kutipan: 'Jalani kehidupan sebaik mungkin, sTay StRong. I love you all',
    )
  ],
);
