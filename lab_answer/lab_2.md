1. Apakah perbedaan JSON dan XML?
- Jawab =
XML, kependekan dari Extensible Markup Language
JSON, kependekan dari JavaScript Object Notation

PARAMETER                       XML                                            JSON   
a. Bahasa           Berasal dari SGML                               Merupakan object JavaScipt
                    (Standard Generalized Markup Language)

b. Penyimpanan      Data XML disimpan sebagai                       Data disimpan seperti map dengan
data                tree structure                                  pasangan

c. Pengolahan       Dapat melakukan pemrosesan dan                  Tidak melakukan pemrosesan atau
                    pemformatan dokumen dan objek                   perhitungan apapun

d. Namespace        Support                                         Tidak support

e. Keamanan         Struktur data XML rentan terhadap               Penguraian JSON aman hampir sepanjang
                    beberapa serangan karena perluasan              waktu jika JSON digunakan, yang dapat
                    entitas eksternal dan validasi  DTD             menyebabkan serangan Cross-Site
                    diaktifkan secara default. Jika ini             Request Forgery (CSRF).
                    dinonaktifkan, pengurai XML akan lebih
                    aman.

f. Tag              Memiliki start dan end tag                      Tidak memiliki start dan end tag 
                                                                    (karena JavaScript tidak 
                                                                    menggunakan tag)

g. Encoding         Bisa menggunakan UTF-8 dan UTF-16 encoding      Hanya bisa UTF-8 encoding

h. Jenis dukungan   Mendukung banyak tipe data kompleks seperti     String, angka, array, boolean, dan
data                bagan, charts, dan tipe data non primitive      object. Objectnya berisi tipe data
                    lainnya.                                        primitive.

2. Apakah perbedaan HTML dan XML?
- Jawab = 
XML, kependekan dari Extensible Markup Language
HTML, kependekan dari Hypertext Markup Language

PARAMETER                       XML                                             HTML
a. Dasar            Menyediakan sebuah framework atau sebuah        Merupakan bahasa markup standar
                    kerangka kerja untuk menentukan bahasa markup

b. Struktural       Informasi pada XML disediakan                   Tidak mengandung informasi struktural

c. Tipe bahasa      Case sensitive                                  Tidak case sensitive

d. Tujuan           Transfer informasi                              Penyajian data

e. Error            Tidak boleh ada error                           Error kecil dapat diabaikan

g. Spasi            Dipertahankan                                   Tidak dipertahankan

h. Tag penutup      Harus ada                                       Optional (bisa ada atau tidak)

i. Nesting atau     Harus dilakukan dengan benar                    Tidak diperhitungkan
urutan