from django.db import models
from django.shortcuts import render, redirect
from lab_2.models import Note
from .forms import NoteForm
def index(request):
    note = Note.objects.all() # TODO Implement this
    response = {'note': note}
    return render(request, 'lab4_index.html', response)
def add_note(request):
    if request.method == 'POST':
        form = NoteForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index-lab4')
    else:
        form = NoteForm()
        response = {'form': form}
        return render(request, 'lab4_form.html', response) 
def note_list(request):
    note = Note.objects.all() # TODO Implement this
    response = {'note': note}
    return render(request, 'lab4_note_list.html', response)

    