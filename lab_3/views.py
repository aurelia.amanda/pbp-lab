from django.shortcuts import redirect, render
from .forms import FriendForm
from lab_1.models import Friend
from django.contrib.auth.decorators import login_required
# Create your views here.
@login_required(login_url='/admin/login/')
def friend_list(request):
    friends = Friend.objects.all() # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    if request.method == 'POST':
        form = FriendForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index-lab3')
    else:
        form = FriendForm()
        response = {'form': form}
        return render(request, 'lab3_form.html', response) 
    