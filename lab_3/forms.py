from django.forms.widgets import DateInput
from lab_1.models import Friend
from django.forms import ModelForm
from django import forms

class FriendForm(ModelForm):
    class Meta:
        model = Friend
        fields = ['name','npm','dob']
    
    class DateInput(forms.DateInput):
        my_date_field = forms.DateField(widget=DateInput)

