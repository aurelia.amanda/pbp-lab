from django.urls import path
from .views import friend_list, add_friend

urlpatterns = [
    path('', friend_list, name='index-lab3'),
    path('add', add_friend, name='form-lab3')
]