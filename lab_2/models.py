from django.db import models

# Create your models here.
class Note(models.Model):
    To = models.CharField(max_length=40)
    From = models.CharField(max_length=40)
    Title = models.CharField(max_length=40)
    Message = models.CharField(max_length=100)
